class ProductDisplay:
    def display(self):
        for product in self._products:
            print(product.display_name())

    def add_product(self, product):
        self._products.append(product)

    def __init__(self):
        self._products = []
